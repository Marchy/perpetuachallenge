//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

import Foundation


class Launch : Decodable {

	//
	// MARK: properties
	//

	let missionPatchImage:( thumbnail:URL, full:URL)?
	let missionName:String
	let launchDate:Date
	let status:Status
	let rocket:Rocket


	//
	// MARK: creation
	//

	init(missionName:String, launchDate:Date, status:Status, rocket:Rocket) {
		self.missionPatchImage = nil
		self.missionName = missionName
		self.launchDate = launchDate
		self.status = status
		self.rocket = rocket
	}
	required init(from decoder:Decoder) throws {
		let json = try decoder.container(keyedBy: CodingKeys.self)
		let linksJSON = try json.nestedContainer(keyedBy: LinksCodingKeys.self, forKey: .links)
		let missionPatchImageURL:String? = try linksJSON.decodeIfPresent(String.self, forKey: .missionPatch)
		let missionPatchSmallImageURL:String? = try linksJSON.decodeIfPresent(String.self, forKey: .missionPatchSmall)
		self.missionPatchImage = missionPatchImageURL.nilOr{ (missionPatchImageURL:String) in
			// ASSUMPTION: If the mission patch is missing, both the full and small values are missing and vice-versa
			(thumbnail: URL(string: missionPatchImageURL)!, full: URL(string: missionPatchSmallImageURL!)!)
		}
		missionName = try json.decode(String.self, forKey: .missionName)
		// TODO: decode date into UTC time here and convert to local timezone upon presentation (format fails to decode in Swift's iso8601 format)
		launchDate = try json.decode(Date.self, forKey: .launchDateLocal)
		let isUpcoming:Bool = try json.decode(Bool.self, forKey: .upcoming)
		let launchSucceeded:Bool? = try json.decodeIfPresent(Bool.self, forKey: .launchSuccess)
		status
			= (isUpcoming) ? .upcoming :
			(launchSucceeded!) ? .successful
			: .failed
		rocket = try json.decode(Rocket.self, forKey: .rocket)
	}


	//
	// MARK: inner structures
	//

	private enum CodingKeys : CodingKey {
		case missionName
		case launchDateLocal
		case launchDateUtc
		case launchDateUnix
		case upcoming
		case launchSuccess
		case rocket
		case links
	}
	private enum LinksCodingKeys : CodingKey {
		case missionPatch
		case missionPatchSmall
	}


	//
	// MARK: outer structure
	//

	enum Status /*: Decodable*/ {
		case successful
		case failed
		case upcoming
	}

}
