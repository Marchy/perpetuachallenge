//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

class Rocket : Decodable {

	//
	// MARK: properties
	//

	let name:String
	let type:String
	let reuse:Reuse


	//
	// MARK: creation
	//

	init(name:String, type:String, reuse:Reuse) {
		self.name = name
		self.type = type
		self.reuse = reuse
	}
	required init(from decoder:Decoder) throws {
		let json:KeyedDecodingContainer<CodingKeys> = try decoder.container(keyedBy: CodingKeys.self)
		name = try json.decode(String.self, forKey: .rocketName)
		type = try json.decode(String.self, forKey: .rocketType)
		// TODO: determine reuse (needs specs clarification: ie: should being reused be considered if any of rocket.fairings.reused, rocket.first_stage.cores[*].reused, or rocket.second_stage.payloads[*].reused are true?
//		let firstStageJSON = rocketJSON.nestedContainer(keyedBy: "fairings", forKey: <#T##CodingKeys##SpaceXChallenge.Rocket.CodingKeys#>)
		reuse = .notApplicable
	}


	//
	// MARK: inner structures
	//

	private enum CodingKeys : String, CodingKey {
		case rocketName
		case rocketType
	}


	//
	// MARK: outer structures
	//

	enum Reuse /*: Decodable*/ {
		case notApplicable
	}

}
