//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

enum APIError : Error {
	// TODO: model different error types
	case miscellaneousError
}
