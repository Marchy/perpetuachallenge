//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

extension Optional {

	public func nilOr<TResult>(_ expression:(Wrapped) -> TResult?) -> TResult? {
		switch self {
		case .none: return nil
		case .some(let value):
			return expression(value)
		}
	}
	public func ifNotNil<TResult>(_ expression:(Wrapped) -> TResult?, otherwise:@autoclosure()->TResult?) -> TResult? {
		switch self {
		case .none: return otherwise()
		case .some(let value):
			return expression(value)
		}
	}

}


@inlinable
@discardableResult
public func with<TObject>( _ object:TObject, block:(TObject) -> () ) -> TObject {
	block( object )

	return object
}
