//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

import Foundation


class API {

	//
	// MARK: properties
	//

	static let decoder = with(JSONDecoder()) {
		$0.keyDecodingStrategy = .convertFromSnakeCase
		$0.dateDecodingStrategy = .iso8601
	}

}
