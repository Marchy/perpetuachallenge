//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

import Foundation


class ServiceLocator {

	//
	// MARK: properties
	//

	public private(set) static var launchService:LaunchService!


	//
	// MARK: creation
	//

	static func initialize(launchService:LaunchService) {
		self.launchService = launchService
	}

}
