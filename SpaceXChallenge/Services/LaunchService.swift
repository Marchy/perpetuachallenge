//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

import Foundation
import Combine


class LaunchService {

	//
	// MARK: properties
	//

	// services
	private var urlSession = URLSession.shared

	// data
	private var cancellables = Set<AnyCancellable>()


	//
	// MARK: operations
	//

	func getLaunches() -> Future<[Launch], Error> {
		let url = URL(string: Constants.baseURL + "launches")!
		return Future<[Launch], Error> { (promise:@escaping (Result<[Launch], Error>) -> ()) in
			self.urlSession.dataTaskPublisher(for: url)
				// TODO: handle API error
				.tryMap{ (data:Data, response:URLResponse) -> Data in
					// TODO: commonalize across operations
					// TODO: handle different error types
					guard let httpResponse = response as? HTTPURLResponse, 200...299 ~= httpResponse.statusCode else {
						throw APIError.miscellaneousError
					}
					
					return data
				}
				.decode(type: [Launch].self, decoder: API.decoder)
				.sink(
					receiveCompletion: { (completion:Subscribers.Completion<Error>) in
						if case let .failure(error) = completion {
							promise(.failure(error))
						}
					}, receiveValue: { launches in
						promise(.success(launches))
					}
				).store(in: &self.cancellables)
		}
	}

}
