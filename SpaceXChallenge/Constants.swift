//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

import Foundation


class Constants {

	//
	// MARK: constants
	//

	static var baseURL:String = "https://api.spacexdata.com/v3/"

}
