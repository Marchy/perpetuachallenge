//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

import UIKit


class RocketDetailsController : UIViewController {

	//
	// MARK: properties
	//

	// views
	@IBOutlet private weak var nameLabel:UILabel!
	@IBOutlet private weak var typeLabel:UILabel!
	@IBOutlet private weak var reuseLabel:UILabel!

	// data
	private var rocket:Rocket!


	//
	// MARK: creation
	//

	func initialize(rocket:Rocket) {
		self.rocket = rocket
	}


	//
	// MARK: overrides
	//

	override func viewDidLoad() {
		super.viewDidLoad()

		// init view
		hydrate()
	}


	//
	// MARK: helpers
	//

	private func hydrate() {
		nameLabel.text = "Rocket Name: \(rocket.name)"
		typeLabel.text = "Rocket Type: \(rocket.type)"
		reuseLabel.text = "Reused: \(rocket.reuse)"
	}

}
