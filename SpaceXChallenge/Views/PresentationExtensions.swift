//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

import Foundation


extension Launch.Status {
	var humanizedText:String {
		switch self {
		case .successful: return "Successful"
		case .failed: return "Failed"
		case .upcoming: return "Upcoming"
		}
	}
}


extension Date {

	//
	// MARK: properties
	//

	// services
	static let dateOnlyDateFormatter = with(DateFormatter()) {
		$0.dateStyle = .full
		$0.timeStyle = .none
	}

	// data
	var humanizedDateOnlyText:String {
		Self.dateOnlyDateFormatter.string(from: self)
	}

}
