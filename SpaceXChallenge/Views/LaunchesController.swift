//
//  LaunchesViewController.swift
//  SpaceXChallenge
//
//  Created by Forrest Chauvin on 2020-03-25.
//  Copyright © 2020 Perpetua Labs, Inc. All rights reserved.
//

import Combine
import UIKit


class LaunchesController : BaseController, UITableViewDataSource, UITableViewDelegate {

	//
	// MARK: constants
	//

	private static let ShowRocketScreenSegueName:String = "ShowRocketScreenSegue"


	//
	// MARK: properties
	//

	// services
	private lazy var launchService:LaunchService = ServiceLocator.launchService

	// views
	@IBOutlet private weak var loadingIndicator:UIActivityIndicatorView!
	@IBOutlet private weak var launchesList:UITableView!
	@IBOutlet private weak var errorLabel:UILabel!

	// data
	private var state:DataState! {
		didSet {
			// hydrate
			if case .loading = state {
				loadingIndicator.startAnimating()
			} else {
				loadingIndicator.stopAnimating()
			}
			launchesList.isHidden = (state != .succeeded)
			errorLabel.isHidden = (state != .failed)
		}
	}
	private var launches:[Launch] = []
	private var cancellables = Set<AnyCancellable>()

	//
	// MARK: overrides
	//

	override func viewDidLoad() {
		super.viewDidLoad()

		// load data
		state = .loading
		launchService.getLaunches()
			.receive(on: RunLoop.main)
			.sink(
				receiveCompletion: { (completion:Subscribers.Completion<Error>) in
					if case let .failure(error) = completion {
						print("ERROR: \(error)")
						// TODO: show error
						self.state = .failed

						// hydrate
						self.errorLabel.text = "OOPS!\nSomething went wrong  :(\nPlease try again later"

					} else {
						self.state = .succeeded
					}
				}, receiveValue: { (launches:[Launch]) in
					self.launches = launches

					// hydrate
					self.launchesList.reloadData()
				}
			).store(in: &cancellables)
	}


	//
	// MARK: implementations
	//

	func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
		launches.count
	}


	func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: LaunchCell.reuseIdentifier, for: indexPath)
			as! LaunchCell
		cell.launch = launches[indexPath.row]

		return cell
	}


	func tableView( _ tableView:UITableView, didSelectRowAt indexPath:IndexPath ){
		// navigate
		let launch:Launch = launches[indexPath.row]
		performSegue(withIdentifier: Self.ShowRocketScreenSegueName, sender: self) { (segue:UIStoryboardSegue) in
			let rocketDetailsController = segue.destination as! RocketDetailsController
			rocketDetailsController.initialize(rocket: launch.rocket)
		}

		// update UI
		tableView.deselectRow(at: indexPath, animated: true)
	}


	//
	// MARK: inner structures
	//

	enum DataState {
		case loading
		case succeeded
		case failed
	}

}
