//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

import UIKit
import SDWebImage


class LaunchCell : UITableViewCell {

	//
	// MARK: constants
	//

	static let reuseIdentifier:String = "LaunchCell"


	//
	// MARK: properties
	//

	// views
	@IBOutlet private weak var launchImageView:UIImageView!
	@IBOutlet private weak var missionNameLabel:UILabel!
	@IBOutlet private weak var launchDateLabel:UILabel!
	@IBOutlet private weak var statusLabel:UILabel!

	// data
	var launch:Launch! {
		didSet {
			hydrate()
		}
	}


	//
	// MARK: helpers
	//

	private func hydrate() {
		launchImageView.sd_setImage(with: launch.missionPatchImage?.thumbnail, placeholderImage: UIImage(named: "launch_placeholder"))
		missionNameLabel.text = launch.missionName
		launchDateLabel.text = launch.launchDate.humanizedDateOnlyText
		statusLabel.text = launch.status.humanizedText
		statusLabel.textColor = { switch launch.status {
			case .successful: return .systemGreen
			case .failed: return .systemRed
			case .upcoming: return .systemBlue
		}}()
	}

}
