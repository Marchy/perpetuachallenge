//
// Created by Marcel Bradea on 2020-09-01.
// Copyright (c) 2020 Perpetua Labs, Inc. All rights reserved.
//

import UIKit


class BaseController : UIViewController {

	//
	// MARK: properties
	//

	private var segueIdentifier:String?
	private var segueInitializer:NavigationHandler?


	//
	// MARK: overrides
	//

	open override func prepare(for segue:UIStoryboardSegue, sender:Any?) {
		if let segueIdentifier:String = segue.identifier {
			guard segueIdentifier == self.segueIdentifier else {
				super.prepare(for: segue, sender: sender)
				return
			}

			// invoke initializer
			if let segueInitializer:NavigationHandler = self.segueInitializer {
				segueInitializer(segue)

				// remove navigation handler
				self.segueIdentifier = nil
				self.segueInitializer = nil
			}
		}

		super.prepare(for: segue, sender: sender)
	}


	//
	// MARK: operations
	//

	open func performSegue(withIdentifier identifier:String, sender:AnyObject?, initialization:@escaping NavigationHandler) {
		// store navigation initialization
		segueIdentifier = identifier
		segueInitializer = initialization

		// navigate
		performSegue(withIdentifier: identifier, sender: sender)
	}


	//
	// MARK: inner structures
	//

	public typealias NavigationHandler = (UIStoryboardSegue) -> ()

}
